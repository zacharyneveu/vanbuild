# Commands reference
+ `cd` - change directory (folder)
+ `..` - one folder above (i.e. `cd ..` moves one folder up)
+ `ls` - list files in current directory
+ `--help` - option to show help for command (i.e. `cd --help` shows how to use cd command)

# Git Commands Quick Start
+ `git clone <url>` - creates a local copy of a web repository
+ `git pull` - gets latest changes from web repository - use before making any changes
+ `git push` - uploads local changes to web repo (origin)
+ `git status` - show status of git
+ `git commit -m "<message>"` - commit work in local copy
+ `git lfs lock <file>` - locks file - use before working on any file
+ `git lfs unlock <file>` - removes file lock - use when done working on file
+ `git lfs unlock <file> --force` - force remove another user's file lock - use with caution
+ `git lfs locks` - lists file locks from all users

# Order of operations (basic)
0. `cd` to `vanbuild` folder
1. `git pull`
2. `git lfs lock <file>`
3. `git lfs locks` - if `lock` command fails
4. Edit and save rhino/stl/png/whatever file you locked
5. `git add <file1> <file2>` or `git add --all`
6. `git status` - optional but useful
7. `git commit -m <message>` 
8. `git push`
9. `git lfs unlock <file>` - releases file for others to work on

`
`

`
