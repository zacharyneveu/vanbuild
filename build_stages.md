# Planning Considerations
## Costs
+ Calculate each build stage approx. costs
+ Calculate van buy cost
+ Estimate time until funds are available for each build stage

## Weight Distribution
+ First, minimize weight wherever possible
+ Plan on towing and account for tongue weight
+ Add up build-in items that weight >50lbs
+ Target ideal axle weight distributions from Ford
+ Target significantly below GVWR

## Layout
+ Create full stage-3 plan before starting build
+ Plan for 2 people


# Build Stages
## Stage 0 - Need in order to use at all (summer only)
+ Vent fan
+ Insulation
+ Wiring/Lighting in place - can be basic at first
+ Flooring
+ Wall panelling/covering
+ Bed

## Stage 1a - Need for comfy KOA trips (lemons weekends)
+ Cabinets - some table-ish space would be really nice
+ Bedside cabinets
+ Gear storage drawers
+ Shore power hookup and corresponding electrical system
+ Propane stove (and propane system & safety)
+ Basic water system - fresh tank, pump, sink

## Stage 1b - Need for winter KOA trips
+ Propane heater
+ Windshield/window (including back) insulation panels

## Stage 2 - Short self sufficient trips (NH weekend, Sugarloaf weekend)
+ Generator/Alternator hookup
+ Batteries
+ [Composting toilet](https://www.natureshead.net/)

## Stage 3 - Longer self sufficient trips (Baxter?)
+ Fridge
+ Outdoor shower system

